package systemebancaire;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */
public class TestBanque {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Banque banque = new Banque("CLC");

        // test creation intiale
        Particulier durand = banque.creerParticulier("Durand", "Robert", "La Rochelle");
        if (durand == null) {
            System.out.println("Probleme sur creation intiale du client Durand");
            System.exit(-1);        
        }
        // test pour verifier que la creation de doublons n'est pas possible
        durand = banque.creerParticulier("Durand", "Robert", "La Rochelle");
        if (durand != null) {
            System.out.println("Probleme sur creation client : doublon");
            System.exit(-1);        
        }
        Particulier martin = banque.creerParticulier("Martin", "Roger", "Nantes");
        if (martin == null) {
            System.out.println("Probleme sur creation intiale d'un client Martin");
            System.exit(-1);        
        }
        CompteBancaireRemunere c1 = banque.creerCompteRemunere(100,5.5,"Durand");
        if (c1 == null) {
            System.out.println("Probleme sur creation intiale d'un compte");
            System.exit(-1);        
        }  
        CompteBancaireRemunere c2 = banque.creerCompteRemunere(0.,4.5,"Martin");
        if (c2 == null) {
            System.out.println("Probleme sur creation intiale d'un compte");
            System.exit(-1);        
        }
        // test pour verifier que la creation d'un compte n'est pas possible
        // lorsque le client n'existe pas...
        CompteBancaireRemunere c3 = banque.creerCompteRemunere(0.,4.5,"Schtroumpf");
        if (c3 != null) {
            System.out.println("Probleme sur creation intiale d'un compte avec client inconnu");
            System.exit(-1);        
        }
        // test pour verifier quu'un client ne peut pas avoir plus de 3 comptes
        //dans la même banque
        Particulier aude = banque.creerParticulier("Javel", "Aude", "Paris");
        CompteBancaireRemunere c4 = banque.creerCompteRemunere(10.,4.5,"Javel");
        CompteBancaireRemunere c5 = banque.creerCompteRemunere(20.,4.5,"Javel");
        CompteBancaireRemunere c6 = banque.creerCompteRemunere(30.,4.5,"Javel");
        //Compte de trop
        CompteBancaireRemunere c7 = banque.creerCompteRemunere(40.,4.5,"Javel");
        if(aude.donneComptes().size() > 3){
            System.out.println("Probleme sur limitation 3 comptes par banque");
            System.exit(-1);       
        }   
        
        // test pour verifier la suppression d'un client
        //Il ne doit plus avoir de compte pour etre supprimé
        if(banque.supprimerClient("Javel")){
            System.out.println("Probleme sur supression client");
            System.exit(-1);       
        }         
        
        ArrayList<CompteBancaire> cpts = new ArrayList<CompteBancaire>();
        cpts.addAll(banque.rechercheCompte("Javel"));
        for (int i = 0; i < cpts.size(); i++) {
            banque.supprimerCompte(cpts.get(i).donneNumero());
        }

        if(!banque.supprimerClient("Javel")){
            System.out.println("Probleme sur supression client");
            System.exit(-1);       
        }   
        
        // test pour verifier la majoration de 5 euros pour un virement entre
        //deux banques differentes
        Banque banque2 = new Banque("V.O.L.E.U.R.S.");
        Particulier jerry = banque.creerParticulier("Goller", "Jerry", "Aubagne");
        Particulier oussama = banque2.creerParticulier("Fayrir", "Oussama", "Toulon");
        
        CompteBancaireRemunere c8 = banque.creerCompteRemunere(100.0,4.5,"Goller");
        CompteBancaireRemunere c9 = banque2.creerCompteRemunere(80,4.5,"Fayrir");
        
        int cptDebiteur = banque.rechercheClient("Goller").donneComptes().get(0).donneNumero();
        int cptCrediteur = banque2.rechercheClient("Fayrir").donneComptes().get(0).donneNumero();
        
        banque.transfertInterBancaire(cptDebiteur, banque2, cptCrediteur, 10);
        
        if(banque.rechercheCompte(cptDebiteur).soldeCourant != 85){
            System.out.println("Probleme sur transfert interbancaire");
            System.exit(-1);       
        }
    }   
}
