package systemebancaire;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */
/**
 * Représente une banque
 */
public class Banque {

    private String nom;
    private ArrayList<Client> clients;
    private ArrayList<CompteBancaire> comptesBancaires;

    /**
     * Creation d'une banque identifiée par son nom
     *
     * @param nomBanque le nom de la banque
     */
    public Banque(String nomBanque) {
        this.nom = nomBanque;
        this.clients = new ArrayList<Client>();
        this.comptesBancaires = new ArrayList<CompteBancaire>();
    }

    /**
     * Création d'un client de type Particulier
     *
     * @param nomParticulier le nom du particulier
     * @param prenom le prenom du particulier
     * @param adresse l'adresse du particulier
     * @return un Particulier ou null si un client de meme nom existe deja
     */
    public Particulier creerParticulier(String nomParticulier, String prenom, String adresse) {
        Particulier p;
        int i = 0;

        //On verifie que le nom n'existe pas déjà      
        while (i < this.clients.size()) {
            if (this.clients.get(i).donneNom().equals(nomParticulier)) {
                return null;
            }
            i++;
        }

        p = new Particulier(nomParticulier, prenom, adresse);
        this.clients.add(p);
        return p;
    }

    /**
     * Création d'un client de type Entreprise
     *
     * @param nomEntreprise le nom de l'entreprise
     * @param numSIRET le numéro siret de l'entreprise
     * @param adresse l'adresse de l'entreprise
     * @return Une entreprise ou null si le nom client existe deja
     */
    public Entreprise creerEntreprise(String nomEntreprise, int numSIRET, String adresse) {
        Entreprise e;
        int i = 0;

        //On verifie que le nom n'existe pas déjà
        while (i < this.clients.size()) {
            if (this.clients.get(i).donneNom().equals(nomEntreprise)) {
                return null;
            }
            i++;
        }

        e = new Entreprise(nomEntreprise, numSIRET, adresse);
        this.clients.add(e);
        return e;
    }

    /**
     * Création d'un compte bancaire au sein de la banque. Le nom du client doit
     * correspondre a un nom de client deja existant
     *
     * @param soldeInitial le solde initial du compte
     * @param nomClient nom du client possédant le compte
     * @return l'instance de CompteBancaire crée ou null en cas de problème
     */
    public CompteBancaire creerCompteBancaire(double soldeInitial, String nomClient) {
        CompteBancaire cpt;
        int i = 0;

        //On verifie que le nom existe
        while (i < this.clients.size()) {
            if (this.clients.get(i).donneNom().equals(nomClient)) {
                cpt = new CompteBancaire(soldeInitial, this.clients.get(i));
                this.clients.get(i).ajouteCompte(cpt);
                this.comptesBancaires.add(cpt);
                return cpt;
            }
            i++;
        }
        return null;
    }

    /**
     * Création d'un compte bancaire rémunéré au sein de la banque. Le nom du
     * client doit correspondre a un nom de client deja existant
     * Et il ne doit pas avoir plus de 3 comptes
     * @param soldeInitial solde initial du compte
     * @param tauxInteret taux d'interet du compte
     * @param nomClient nom du client possédant le compte
     * @return l'instance de CompteBancaire crée ou null en cas de problème
     */
    public CompteBancaireRemunere creerCompteRemunere(double soldeInitial, double tauxInteret, String nomClient) {
        CompteBancaireRemunere cptRemu;
        int i = 0;

        //On verifie que le nom existe
        while (i < this.clients.size()) {
            if (this.clients.get(i).donneNom().equals(nomClient)) {
                if(this.rechercheCompte(nomClient).size() < 3){
                    cptRemu = new CompteBancaireRemunere(soldeInitial, tauxInteret, this.clients.get(i));
                    this.clients.get(i).ajouteCompte(cptRemu);
                    this.comptesBancaires.add(cptRemu);
                    return cptRemu;
                }               
            }
            i++;
        }
        return null;
    }

    /**
     * Donne l'ensemble des comptes associés à un client identifié par son nom
     *
     * @param nomClient le nom du client
     * @return une liste des comptes appartenant au client
     */
    public ArrayList<CompteBancaire> rechercheCompte(String nomClient) {
        ArrayList<CompteBancaire> comptesClient = null;

        for (int i = 0; i < this.clients.size(); i++) {
            if (this.clients.get(i).donneNom().equals(nomClient)) {
                comptesClient = this.clients.get(i).donneComptes();
            }
        }
        return comptesClient;
    }

    /**
     * Donne le compte bancaire, s'il existe, possédant le numéro en parametre
     *
     * @param numeroCpte le numéro du compte recherché
     * @return l'instance de CompteBancaire recherchée ou null si n'existe pas.
     */
    public CompteBancaire rechercheCompte(int numeroCpte) {
        CompteBancaire compte = null;
        int i = 0;

        while (i < this.comptesBancaires.size()) {
            if (this.comptesBancaires.get(i).donneNumero() == numeroCpte) {
                return this.comptesBancaires.get(i);
            }
            i++;
        }
        return compte;
    }

    /**
     * Supprime un compte bancaire, s'il existe, possédant le numéro en
     * parametre
     *
     * @param numeroCpte numero du compte
     * @return true si la suppression a été effectuée false s'il y a un probleme
     */
    public boolean supprimerCompte(int numeroCpte) {
        boolean suppr = false;
        int i = 0;

        while (i < this.comptesBancaires.size()) {
            if (this.comptesBancaires.get(i).donneNumero() == numeroCpte) {
                this.comptesBancaires.get(i).donneDetenteur().supprimeCompte(numeroCpte);
                this.comptesBancaires.remove(i);
                return true;
            }
            i++;
        }
        return suppr;
    }

    /**
     * Recherche un client de la banque à partir de son nom
     *
     * @param nomClient nom du client à rechercher
     * @return le client correspondant ou null s'il n'existe pas
     */
    public Client rechercheClient(String nomClient) {
        Client cl = null;
        int i = 0;

        while (i < this.clients.size()) {
            if (this.clients.get(i).donneNom() == nomClient) {
                return this.clients.get(i);
            }
            i++;
        }
        return cl;
    }

    /**
     * Supprime un client de la banque. Supprime uniquement si le client ne
     * possede plus de compte
     *
     * @param nomClient nom du client
     * @return true si la suppression est ok ou false si il y a un probleme
     */
    public boolean supprimerClient(String nomClient) {
        boolean suppr = false;
        int i = 0;

        while (i < this.clients.size()) {
            if (this.clients.get(i).donneNom() == nomClient) {
                //On verifie que le client n'a plus de compte
                if (this.clients.get(i).donneComptes().isEmpty()) {
                    this.clients.remove(i);
                    return true;
                } else {
                    System.out.println("Le client " + this.clients.get(i).donneNom() + " possède encore au moins un compte");
                    System.out.println("Suppression impossible");
                    return false;
                }
            }
            i++;
        }
        return suppr;
    }

    /**
     * Transfert un montant d'un compte vers un autre compte
     *
     * @param numeroCpteDebiteur numero du compte débité
     * @param banqueCrediteur banque du compte crédité
     * @param numeroCpteCrediteur numero du compte crédité
     * @param montant montant du transfert
     * @return true si le transfert a été effectué false s'il y a un probleme
     */
    public boolean transfertInterBancaire(int numeroCpteDebiteur, Banque banqueCrediteur, int numeroCpteCrediteur, double montant) {
        boolean transfert = false;
        //Chercher le compte qui va etre debité
        CompteBancaire debiteur = this.rechercheCompte(numeroCpteDebiteur);

        //Chercher le compte qui va etre credité (de banqueCrediteur)
        CompteBancaire crediteur = banqueCrediteur.rechercheCompte(numeroCpteCrediteur);

        //Le compte débiteur transfert vers le compte créditeur;  
        if (debiteur.transferer(crediteur, montant)) {
            if(banqueCrediteur != this){
                this.rechercheCompte(numeroCpteDebiteur).debiter(5);//majoration
            }
            return true;
        }

        return transfert;
    }

    public String toString() {
        return "Nom de la banque : " + this.nom
                + "\nClients : " + this.clients.toString()
                + "\nComptes bancaires : " + this.comptesBancaires.toString();
    }
}
