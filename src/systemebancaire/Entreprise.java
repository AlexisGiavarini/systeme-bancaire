package systemebancaire;

/**
 *
 * @author Alexis
 */

/**
 * Représente une entreprise cliente d'une banque
 */
public class Entreprise extends Client{
    private int siret;
    
    /**
     * Crée une entreprise avec des informations
     * @param nom nom de l'entreprise
     * @param numSiret numSiret de l'entreprise
     * @param adresse adresse de l'entreprise
     */
    public Entreprise(String nom, int numSiret, String adresse){
        super(nom, adresse);
        this.siret = numSiret;
    }
    
    /**
     * Retourne le numéro siret de l'entreprise
     * @return le numéro siret
     */
    public int donneSiret(){
        return this.siret;
    }
    
    public String toString(){
        return super.toString()+"\n\tNuméro Siret : "+this.siret;
    }
}
