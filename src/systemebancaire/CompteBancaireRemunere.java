package systemebancaire;

/**
 *
 * @author Alexis
 */

/**
 * Represente un compte bancaire rémunéré
 */
public class CompteBancaireRemunere extends CompteBancaire {
    private static double interetParDefaut = 3;
    private double tauxRemuneration = interetParDefaut;
    
    
    /**
     * Permet de créer un compte bancaire rémunéré en lui attribuant
     * automatiquement un numéro
     * @param soldeInitial valeur du solde initial
     * @param tauxInteret taux d'interet de la remuneration
     * @param c client auquel le compte est rattaché
     */
    public CompteBancaireRemunere(double soldeInitial, double tauxInteret,Client c){
        super(soldeInitial,c);
        this.tauxRemuneration = tauxInteret;       
    }
    
    /**
     * Crée un compte bancaire rémunéré dont le taux d'interet est celui
     * par defaut et dont le solde initial est à zéro
     * @param numero le numero du compte
     * @param c client auquel le compte est rattaché
     */
    public CompteBancaireRemunere(int numero, Client c){
        super(numero,c);
    }
    
    /**
     * Crée un compte bancaire rémunéré dont le taux d'intéret est celui 
     * par defaut
     * @param numero le numero du compte
     * @param soldeInitial valeur du solde initial
     * @param c client auquel le compte est rattaché
     */
    public CompteBancaireRemunere(int numero, double soldeInitial, Client c){
        super(numero, soldeInitial,c);
    }
    /**
     * Crée un compte bancaire rémunéré
     * @param numero le numero du compte
     * @param soldeInitial valeur du solde initial
     * @param tauxInteret taux d'interet de la remuneration
     * @param c client auquel le compte est rattaché
     */
    public CompteBancaireRemunere(int numero, double soldeInitial,double tauxInteret, Client c){
        super(numero, soldeInitial,c);
        this.tauxRemuneration = tauxInteret;
    }
    
    /**
     * Crédite le compte des intérets mensuels
     */
    public void crediterInteretMensuel(){
        double interetAnnuel = this.soldeCourant/100 * this.tauxRemuneration;
        this.soldeCourant += interetAnnuel/12;
    }
    
    public String toString(){
        return super.toString()+"\nTaux interets : "+this.tauxRemuneration;
    }
}
