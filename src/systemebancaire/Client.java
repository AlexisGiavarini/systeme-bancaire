package systemebancaire;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */

/**
 * Représente un client d'un banque
 */
public class Client {

    private String nom;
    private String adresse;
    private ArrayList<CompteBancaire> comptes;

    /**
     * Crée un client avec des informations personnelles
     */
    public Client(String nom, String adresse) {
        this.nom = nom;
        this.adresse = adresse;
        this.comptes = new ArrayList<CompteBancaire>();
    }
    
    /**
     * Retourne le nom du client
     */
    public String donneNom(){
        return this.nom;
    }
    
    /**
     * Retourne l'adresse du client
     */
    public String donneAdresse(){
        return this.adresse;
    }
    
    /**
     * Modifie l'adresse du client
     */
    public void changeAdresse(String adresse){
        this.adresse = adresse;
    }
    
    /**
     * Attribue un compte à un client
     * @param compte compte à attribuer
     * @return false si le compte est deja attribué, true sinon
     */
    public boolean ajouteCompte(CompteBancaire compte){
        boolean ajout = true;
        int i = 0;
        
        while (i < this.comptes.size()) {
            if(this.comptes.get(i).donneNumero() == compte.donneNumero()){
                return false;
            }
            i++;
        }
        if(ajout){
            this.comptes.add(compte);
        }
        return ajout;
    }
    
    /**
     * Supprime le compte du client
     * @param numCompte numéro du compte à supprimer
     * @return true si le compte existe, sinon false
     */
    public boolean supprimeCompte(int numCompte){
        boolean existe = false;
        int i = 0;
        
        while (i < this.comptes.size()) {
            if(this.comptes.get(i).donneNumero() == numCompte){
                this.comptes.remove(i);
                return true;
            }
            i++;
        }
        return existe;
    }
    
    /**
     * Retourne l'ensemble des comptes du client
     * @return la liste des comptes du client
     */
    public ArrayList<CompteBancaire> donneComptes(){
        return this.comptes;
    }
    
    public String toString(){
        return "\n\tNom : "+this.donneNom()+"\n\tAdresse : "+this.donneAdresse();
    }
}
