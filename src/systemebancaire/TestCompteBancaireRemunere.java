package systemebancaire;

/**
 *
 * @author Alexis
 */
public class TestCompteBancaireRemunere {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Client durand = new Client("Durand", "La Rochelle");
        Client martin = new Client("Martin", "Nantes");
        Client alexis = new Client("Alexis", "Colomiers");
        Client renan = new Client("Renan", "Albi");

        CompteBancaireRemunere c1 = new CompteBancaireRemunere(1,100,5.5,durand);
        CompteBancaireRemunere c2 = new CompteBancaireRemunere(2,0,martin);
        CompteBancaireRemunere c3 = new CompteBancaireRemunere(3,alexis);
        //Numero de Renan vaut bien 0 car dans classe numero = nb
        CompteBancaireRemunere c4 = new CompteBancaireRemunere(200.0,4.5,renan);
        
        // necessaire pour associer Client -> Compte
        if (!martin.ajouteCompte(c2)) {
            System.out.println("Probleme sur l'attribution du compte a : " + martin);
            System.exit(-1);                
        }
        if (!alexis.ajouteCompte(c3)) {
            System.out.println("Probleme sur l'attribution du compte a : " + alexis);
            System.exit(-1);                
        }
        if (!renan.ajouteCompte(c4)) {
            System.out.println("Probleme sur l'attribution du compte a : " + renan);
            System.exit(-1);                
        }
        
        // test pour verifier qu'on ne peut pas attribuer 2 fois le meme compte
        if (martin.ajouteCompte(c2)) {
            System.out.println("Probleme compte attribue deux fois a : " + martin);
            System.exit(-1);                
        }
        if (alexis.ajouteCompte(c3)) {
            System.out.println("Probleme compte attribue deux fois a : " + alexis);
            System.exit(-1);                
        }
        if (renan.ajouteCompte(c4)) {
            System.out.println("Probleme compte attribue deux fois a : " + renan);
            System.exit(-1);                
        }

        // test pour verifier la methode creditInteretMensuel
        double solde_c2_attendu = 0;
        c2.crediterInteretMensuel();
        if (c2.consulter() != solde_c2_attendu) {
            System.out.println("Probleme sur interets mensuels de " + c2);
            System.exit(-1);
        }      
        double solde_c3_attendu = 0;
        c3.crediterInteretMensuel();
        if (c3.consulter() != solde_c3_attendu) {
            System.out.println("Probleme sur interets mensuels de " + c3);
            System.exit(-1);
        } 
        double solde_c4_attendu = 200.75;
        c4.crediterInteretMensuel();
        if (c4.consulter() != solde_c4_attendu) {
            System.out.println("Probleme sur interets mensuels de " + c4);
            System.exit(-1);
        } 
        
        Client alexandre = new Client("Alexandre", "Toulouse");
        Client alyzée = new Client("Alyzée", "Toulouse");
        CompteBancaire c5 = new CompteBancaire(80.0,alexandre);
        CompteBancaire c6 = new CompteBancaire(39.0,alyzée);
        alexandre.ajouteCompte(c5); //numero vaut 1
        alyzée.ajouteCompte(c6);//numero vaut 2
    } 
}
