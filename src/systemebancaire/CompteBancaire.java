package systemebancaire;

/**
 *
 * @author Alexis
 */
/**
 * Représente un compte bancaire (simplifié)
 */
public class CompteBancaire {

    private static int nb = 0;
    protected int numero = nb;
    protected double soldeInitial;
    protected double soldeCourant;
    protected Client client;

    /**
     * Permet de créer un compte en lui attribuant automatiquement un numéro
     */
    public CompteBancaire(double soldeInitial, Client client) {
        CompteBancaire.increNum();
        this.soldeInitial = soldeInitial;
        this.soldeCourant = soldeInitial;
        this.client = client;
    }

    /**
     * Permet de créer un compte en précisant le numéro et le détenteur
     */
    public CompteBancaire(int numero,Client client) {
        this.numero = numero;
        this.client = client;
        this.soldeInitial = 0; 
        this.soldeCourant = this.soldeInitial;
    }

    /**
     * Permet de créer un compte en précisant le numéro, le solde intial et le détenteur
     */
    public CompteBancaire(int numero, double soldeInitial, Client client) {
        this.numero = numero;
        this.soldeInitial = soldeInitial;
        this.soldeCourant = soldeInitial;
        this.client = client;
    }
    
    /**
     * Retourne le numéro du compte
     * @return numero du compte
     */
    public int donneNumero(){
        return this.numero;
    }
    
    /**
     * Retourne le détenteur du compte
     * @return le client détenteur du compte
     */
    public Client donneDetenteur(){
        return this.client;
    }
    
    /**
     * Crédite le compte du montant indiqué
     * @return le solde du compte apres crédit
     * @param montant montant à créditer
     */
    public double crediter(double montant){
        this.soldeCourant += montant;
       return this.soldeCourant;
    }
    
    /**
     * Débite le compte du montant indiqué
     * @return le solde du compte apres débit
     * @param montant montant à débiter
     */
    public double debiter(double montant){
       this.soldeCourant -= montant;
       return this.soldeCourant;
    }
    
    /**
     * Retourne le solde courant du compte
     * @return le solde du compte
     */
    public double consulter(){
        return this.soldeCourant;
    }
    
    /**
     * Transfert le montant vers le compte indiqué.
     * Le transfert ne doit pas générer un découvert.
     * Si tel est le cas, il n'est pas accepté
     * @return true si le transfert a pu s'effectuer, false sinon
     * @param unCompte compte à créditer
     * @param montantATransferer montant à débiter
     */
    public boolean transferer(CompteBancaire unCompte, double montantATransferer){
        boolean transfert = true;
        
        if(this.debiter(montantATransferer) >= 0){
            unCompte.crediter(montantATransferer);
            //this.debiter(montantATransferer);            
        }else{
            transfert = false;
            System.out.println("Transfert refusé, fonds insuffisants");
        }
        
        return transfert;
    }
    
    /**
     * Donne le un numero à chacun des comptes
     */
    private static void increNum(){
        CompteBancaire.nb++;
    }
    
    public String toString(){
        return "Compte numéro "+this.donneNumero()
                +"\nSolde initial : "+this.soldeInitial
                +"\nSolde courant : "+this.consulter()
                +"\nClient : "+this.donneDetenteur().toString();
    }
}
