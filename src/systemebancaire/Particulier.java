package systemebancaire;

/**
 *
 * @author Alexis
 */

/**
 * Représente un particulier client d'une banque
 */
public class Particulier extends Client {
    private String prenom;
    
    /**
     * Crée un particulier avec des informations personnelles
     * @param nom nom du particulier
     * @param prenom prenom du particulier
     * @param adresse adresse du particulier
     */
    public Particulier(String nom, String prenom, String adresse){
        super(nom,adresse);
        this.prenom = prenom;
    }
    
    /**
     * retourne le prenom du particulier
     * @return le prenom du particulier
     */
    public String donnePrenom(){
        return this.prenom;
    }
    
    public String toString(){
        return super.toString()+"\n\tPrénom : "+this.prenom;
    }
}
